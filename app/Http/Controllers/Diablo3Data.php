<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Classes\CustomProvider;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class Diablo3Data extends Controller
{
    protected $layout = 'layouts.test';
	
	/*** Diablo3Data Views ***/
	public function diablo3CareerProfiles_view(Request $request)
	{
		return response()->view('layouts/diablo3CareerProfiles');
	}
	
	public function diablo3HeroProfiles_view(Request $request)
	{
		return response()->view('layouts/diablo3HeroProfiles');
	}
	
	public function diablo3Items_view(Request $request)
	{
		return response()->view('layouts/diablo3Items');
	}
	
	public function diablo3Follower_view(Request $request)
	{
		return response()->view('layouts/diablo3Follower');
	}
	
	public function diablo3Artisan_view(Request $request)
	{
		return response()->view('layouts/diablo3Artisan');
	}
}