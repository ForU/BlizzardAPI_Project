<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Classes\CustomProvider;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class SC2Data extends Controller
{
    protected $layout = 'layouts.test';
	
	/*** WowData Views ***/
	public function sc2Profiles_view(Request $request)
	{
		return response()->view('layouts/sc2Profiles');
	}
	
	public function sc2Ladders_view(Request $request)
	{
		return response()->view('layouts/sc2Ladders');
	}
	
	public function sc2MatchHistory_view(Request $request)
	{
		return response()->view('layouts/sc2MatchHistory');
	}
	
	public function sc2Ladder_view(Request $request)
	{
		return response()->view('layouts/sc2Ladder');
	}
	
	public function sc2AchievementsData_view(Request $request)
	{
		return response()->view('layouts/sc2AchievementsData');
	}
	
	public function sc2RewardsData_view(Request $request)
	{
		return response()->view('layouts/sc2RewardsData');
	}
}