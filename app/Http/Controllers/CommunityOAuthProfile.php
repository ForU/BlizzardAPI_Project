<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use OAuth;
use App\Custom_Classes\CustomProvider;

class CommunityOAuthProfile extends Controller
{
	public function wowProfile(Request $request)
    {	
		$CustomProvider = new CustomProvider;
		$provider = $CustomProvider->initWowProvider();
		
		if (!isset($_GET['code']))
		{
			
			$auth_url = $provider->getAuthorizationUrl();
			$stateString = $provider->getState();
			header('Location: ' . $auth_url);
			die('Redirect');
		}
		elseif(empty($_GET['state']))
		{
			exit('Invalid state');
		}
		else
		{
			$accessToken = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.battle.net/oauth/check_token?token=" . $accessToken,
				CURLOPT_HEADER => false,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			$obj = json_decode($content);
			
			if(array_key_exists('client_id', $obj))
			{
				echo "Token is valid!";
				echo "<pre>";
				echo $accessToken->getToken() . "\n";
				echo $accessToken->getRefreshToken() . "\n";
				echo $accessToken->getExpires() . "\n";
				echo ($accessToken->hasExpired() ? 'expired' : 'not expired') . "\n";
				$resourceOwner = $provider->getResourceOwner($accessToken);
				var_export($resourceOwner->toArray());
				echo "</pre>";
			}
			else
			{
				return "Token is invalid!";
			}
		}
    }
	
	public function sc2Profile(Request $request)
    {	
		$CustomProvider = new CustomProvider;
		$provider = $CustomProvider->initSc2Provider();
		
		if (!isset($_GET['code']))
		{
			
			$auth_url = $provider->getAuthorizationUrl();
			$stateString = $provider->getState();
			header('Location: ' . $auth_url);
			die('Redirect');
		}
		elseif(empty($_GET['state']))
		{
			exit('Invalid state');
		}
		else
		{
			$accessToken = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);

			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.battle.net/oauth/check_token?token=" . $accessToken,
				CURLOPT_HEADER => false,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			$obj = json_decode($content);
			
			if(array_key_exists('client_id', $obj))
			{
				echo "Token is valid!";
				echo "<pre>";
				echo $accessToken->getToken() . "\n";
				echo $accessToken->getRefreshToken() . "\n";
				echo $accessToken->getExpires() . "\n";
				echo ($accessToken->hasExpired() ? 'expired' : 'not expired') . "\n";
				$resourceOwner = $provider->getResourceOwner($accessToken);
				var_export($resourceOwner->toArray());
				echo "</pre>";
			}
			else
			{
				return "Token is invalid!";
			}
		}
    }
	
	public function acctInfo(Request $request)
    {	
		$CustomProvider = new CustomProvider;
		$provider = $CustomProvider->initAccountProvider();
		
		if (!isset($_GET['code']))
		{
			
			$auth_url = $provider->getAuthorizationUrl();
			$stateString = $provider->getState();
			header('Location: ' . $auth_url);
			die('Redirect');
		}
		elseif(empty($_GET['state']))
		{
			exit('Invalid state');
		}
		else
		{
			$accessToken = $provider->getAccessToken('authorization_code', ['code' => $_GET['code']]);

			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.battle.net/oauth/check_token?token=" . $accessToken,
				CURLOPT_HEADER => false,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			$obj = json_decode($content);
			
			if(array_key_exists('client_id', $obj))
			{
				echo "Token is valid!";
				echo "<pre>";
				echo $accessToken->getToken() . "\n";
				echo $accessToken->getRefreshToken() . "\n";
				echo $accessToken->getExpires() . "\n";
				echo ($accessToken->hasExpired() ? 'expired' : 'not expired') . "\n";
				$resourceOwner = $provider->getResourceOwner($accessToken);
				var_export($resourceOwner->toArray());
				echo "</pre>";
			}
			else
			{
				return "Token is invalid!";
			}
		}
    }
}