<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Classes\CustomProvider;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class GeneralData extends Controller
{
    protected $layout = 'layouts.test';
	
	/*** GeneralData Views ***/
	public function apiList_view(Request $request)
	{
		return response()->view('layouts/apiList');
	}
}