<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Classes\CustomProvider;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class WoWData extends Controller
{	
	protected $layout = 'layouts.test';
	
	/*** WowData Views ***/
	public function achievement_view(Request $request)
	{
		return response()->view('layouts/achievement');
	}
	
	public function auction_view(Request $request)
	{
		return response()->view('layouts/auction');
	}
	
	public function bossMasterList_view(Request $request)
	{
		return response()->view('layouts/bossMasterList');
	}
	
	public function bossInfo_view(Request $request)
	{
		return response()->view('layouts/bossInfo');
	}
	
	public function petMasterList_view(Request $request)
	{
		return response()->view('layouts/petMasterList');
	}
	
	public function petAbility_view(Request $request)
	{
		return response()->view('layouts/petAbility');
	}
	
	public function petSpecies_view(Request $request)
	{
		return response()->view('layouts/petSpecies');
	}
	
	public function petStats_view(Request $request)
	{
		return response()->view('layouts/petStats');
	}
	
	public function mountList_view(Request $request)
	{
		return response()->view('layouts/mountList');
	}
	
	public function challengeRealm_view(Request $request)
	{
		return response()->view('layouts/challengeRealm');
	}
	
	public function challengeRegion_view(Request $request)
	{
		return response()->view('layouts/challengeRegion');
	}
	
	public function characterProfile_view(Request $request)
	{
		return response()->view('layouts/characterProfile');
	}
	
	public function characterAchievements_view(Request $request)
	{
		return response()->view('layouts/characterAchievements');
	}
	
	public function characterAppearance_view(Request $request)
	{
		return response()->view('layouts/characterAppearance');
	}
	
	public function characterFeed_view(Request $request)
	{
		return response()->view('layouts/characterFeed');
	}
	
	public function characterGuild_view(Request $request)
	{
		return response()->view('layouts/characterGuild');
	}
	
	public function characterHunterPets_view(Request $request)
	{
		return response()->view('layouts/characterHunterPets');
	}
	
	public function characterItems_view(Request $request)
	{
		return response()->view('layouts/characterItems');
	}
	
	public function characterMounts_view(Request $request)
	{
		return response()->view('layouts/characterMounts');
	}
	
	public function characterPets_view(Request $request)
	{
		return response()->view('layouts/characterPets');
	}
	
	public function characterPetSlots_view(Request $request)
	{
		return response()->view('layouts/characterPetSlots');
	}
	
	public function characterProfessions_view(Request $request)
	{
		return response()->view('layouts/characterProfessions');
	}
	
	public function characterProgression_view(Request $request)
	{
		return response()->view('layouts/characterProgression');
	}
	
	public function characterPVP_view(Request $request)
	{
		return response()->view('layouts/characterPVP');
	}
	
	public function characterQuests_view(Request $request)
	{
		return response()->view('layouts/characterQuests');
	}
	
	public function characterReputation_view(Request $request)
	{
		return response()->view('layouts/characterReputation');
	}
	
	public function characterStatistics_view(Request $request)
	{
		return response()->view('layouts/characterStatistics');
	}
	
	public function characterStats_view(Request $request)
	{
		return response()->view('layouts/characterStats');
	}
	
	public function characterTalents_view(Request $request)
	{
		return response()->view('layouts/characterTalents');
	}
	
	public function characterTitles_view(Request $request)
	{
		return response()->view('layouts/characterTitles');
	}
	
	public function characterAudit_view(Request $request)
	{
		return response()->view('layouts/characterAudit');
	}
	
	public function itemId_view(Request $request)
	{
		return response()->view('layouts/itemId');
	}
	
	public function itemSetId_view(Request $request)
	{
		return response()->view('layouts/itemSetId');
	}
	
	public function guildProfile_view(Request $request)
	{
		return response()->view('layouts/guildProfile');
	}
	
	public function guildMembers_view(Request $request)
	{
		return response()->view('layouts/guildMembers');
	}
	
	public function guildAchievements_view(Request $request)
	{
		return response()->view('layouts/guildAchievements');
	}
	
	public function guildNews_view(Request $request)
	{
		return response()->view('layouts/guildNews');
	}
	
	public function guildChallenge_view(Request $request)
	{
		return response()->view('layouts/guildChallenge');
	}
	
	public function pvpLeaderboards_view(Request $request)
	{
		return response()->view('layouts/pvpLeaderboards');
	}
	
	public function quest_view(Request $request)
	{
		return response()->view('layouts/quest');
	}
	
	public function realmStatus_view(Request $request)
	{
		return response()->view('layouts/realmStatus');
	}
	
	public function recipe_view(Request $request)
	{
		return response()->view('layouts/recipe');
	}
	
	public function spell_view(Request $request)
	{
		return response()->view('layouts/spell');
	}
	
	public function zoneMasterList_view(Request $request)
	{
		return response()->view('layouts/zoneMasterList');
	}
	
	public function zone_view(Request $request)
	{
		return response()->view('layouts/zone');
	}
	
	public function battleGroups_view(Request $request)
	{
		return response()->view('layouts/battleGroups');
	}
	
	public function characterRacesResources_view(Request $request)
	{
		return response()->view('layouts/characterRacesResources');
	}
	
	public function characterClassesResources_view(Request $request)
	{
		return response()->view('layouts/characterClassesResources');
	}
	
	public function characterAchievementsResources_view(Request $request)
	{
		return response()->view('layouts/characterAchievementsResources');
	}
	
	public function guildRewardsResources_view(Request $request)
	{
		return response()->view('layouts/guildRewardsResources');
	}
	
	public function guildPerksResources_view(Request $request)
	{
		return response()->view('layouts/guildPerksResources');
	}
	
	public function guildAchievementsResources_view(Request $request)
	{
		return response()->view('layouts/guildAchievementsResources');
	}
	
	public function itemClassesResources_view(Request $request)
	{
		return response()->view('layouts/itemClassesResources');
	}
	
	public function talentsResources_view(Request $request)
	{
		return response()->view('layouts/talentsResources');
	}
	
	public function petTypesResources_view(Request $request)
	{
		return response()->view('layouts/petTypesResources');
	}
	
	/*** !WowData Views! ***/
	
	/**
	@return array
	*/
	// public function achievements(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/achievement/2144?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
	
			// return response()->view('layouts/test', ['content' => $obj]);
			
	// }
	
	// public function auction(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/auction/data/medivh?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function petMasterList(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/pet/?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function battlePetAbility(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/battlePet/ability/640?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function battlePetSpecies(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/battlePet/species/258?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function battlePetStats(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/battlePet/stats/258?level=25&breedId=5&qualityId=4&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowChallengeRealm(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/challenge/medivh?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowChallengeRegion(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/challenge/region?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterProfile(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/Fenris/john?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterAchievements(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=achievements&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterAppearance(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=appearance&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterFeed(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=feed&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterGuild(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=guild&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterHunterPets(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=hunterPets&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterItems(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=items&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterMounts(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=mounts&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterPets(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=pets&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterPetSlots(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=petSlots&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterProfessions(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=professions&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterProgression(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=progression&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterPvp(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=pvp&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterQuests(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=quests&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterReputation(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=reputation&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterStatistics(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=statistics&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterStats(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=stats&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterTalents(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=talents&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterTitles(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=titles&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowCharacterAudit(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/character/fenris/john?fields=audit&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowItemId(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/item/18803?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowItemSetId(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/item/set/1060?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowGuildProfile(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/guild/Exodar/Halo?fields=achievements%2Cchallenge&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowGuildMembers(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/guild/Exodar/Halo?fields=members&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowGuildAchievements(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/guild/Exodar/Halo?fields=achievements&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowGuildNews(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/guild/Exodar/Halo?fields=news&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowGuildChallenge(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/guild/Exodar/Halo?fields=challenge&locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowPVPLeaderboards(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/leaderboard/2v2?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowQuestId(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/quest/13146?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowRealmStatus(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/realm/status?locale=en_US&apikey=" .$apiKey. "",
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowRecipeId(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/recipe/33994?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowSpellId(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/spell/8056?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataBattleGroups(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/battlegroups/?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataCharacterRaces(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/character/races?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataCharacterClasses(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/character/classes?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataCharacterAchievements(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/character/achievements?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataGuildRewards(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/guild/rewards?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataGuildPerks(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/guild/perks?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataGuildAchievements(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/guild/achievements?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataItemClasses(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/item/classes?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataTalents(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/talents?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
	
	// public function wowDataPetTypes(Request $request)
    // {
		// $CustomProvider = new CustomProvider;
		// $apiKey = $CustomProvider->initApiKey();
		
		// $ch = curl_init();
			// $options = array(
				// CURLOPT_URL => "https://us.api.battle.net/wow/data/pet/types?locale=en_US&apikey=" .$apiKey. "",
				
				// CURLOPT_HEADER => false,
				// CURLOPT_FOLLOWLOCATION => true,
				// CURLOPT_RETURNTRANSFER => true
			// );

			// curl_setopt_array($ch, $options);
			// $content = curl_exec($ch);
			// curl_close($ch);
			
			// $obj = json_decode($content);
			
			// return response(view('layouts/test', ['content' => $obj]));
	// }
}
