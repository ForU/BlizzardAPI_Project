<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom_Classes\CustomProvider;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class ajaxFunctions extends Controller
{	
	// protected $layout = 'layouts.test';
	
	public function achievement_ctrl(Request $request)
	// public function achievement_ctrl()
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		// $id = Input::post('id');
		// return $id;
	
		if(isset($_POST['id']) && isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$id = $_POST['id'];
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/achievement/".$id."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			$obj = json_decode($content);

			// return response()->view('layouts/test', ['content' => $obj]);	
			// return response($content);	
			// return response()->json([$obj], 200);
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
		// return response("Jeane");
		// return response()->json(['Jeanie'=>'true'], 200);
	}
	
	public function auction_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
	
		if(isset($_POST['realm']) && isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$realm = $_POST['realm'];
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/auction/data/".$realm."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			// $obj = json_decode($content);
			// return response()->json([$obj], 200);
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function bossMasterList_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/boss/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			// $obj = json_decode($content);
			// return response()->json([$obj], 200);
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function bossInfo_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['bossId']) && isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$bossId = $_POST['bossId'];
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/boss/".$bossId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			// $obj = json_decode($content);
			// return response()->json([$obj], 200);
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function petMasterList_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
	
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/pet/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			// $obj = json_decode($content);
			// return response()->json([$obj], 200);
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function petAbility_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
	
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['abilityId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$abilityId = $_POST['abilityId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/pet/ability/".$abilityId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			// $obj = json_decode($content);
			// return response()->json([$obj], 200);
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function petSpecies_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['speciesId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$speciesId = $_POST['speciesId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/pet/species/".$speciesId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function petStats_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['speciesId']) && isset($_POST['level']) && isset($_POST['breedId']) && isset($_POST['qualityId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$speciesId = $_POST['speciesId'];
			$level = $_POST['level'];
			$breedId = $_POST['breedId'];
			$qualityId = $_POST['qualityId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/pet/stats/".$speciesId."?level=".$level."&breedId=".$breedId."&qualityId=".$qualityId."&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function mountList_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/mount/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function challengeRealm_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/challenge/".$realm."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function challengeRegion_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/challenge/region?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterProfile_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']) && isset($_POST['profileFields']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			$profileFields = $_POST['profileFields'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=".$profileFields."&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterAchievements_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=achievements&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterAppearance_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=appearance&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterFeed_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=feed&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterGuild_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=guild&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterHunterPets_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=hunterPets&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterItems_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=items&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterMounts_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=mounts&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterPets_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=pets&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterPetSlots_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=petSlots&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterProfessions_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=professions&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterProgression_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=progression&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterPVP_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=pvp&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterQuests_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=quests&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterReputation_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=reputation&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterStatistics_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=statistics&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterStats_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=stats&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterTalents_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=talents&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterTitles_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=titles&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function characterAudit_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['characterName']) && isset($_POST['realm']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$characterName = $_POST['characterName'];
			$realm = $_POST['realm'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/character/".$realm."/".$characterName."?fields=audit&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function itemId_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['itemId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$itemId = $_POST['itemId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/item/".$itemId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function itemSetId_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['itemSetId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$itemSetId = $_POST['itemSetId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/item/set/".$itemSetId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function guildProfile_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['realm']) && isset($_POST['guildName']) && isset($_POST['guildFields']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$realm = $_POST['realm'];
			$guildName = $_POST['guildName'];
			$guildFields = $_POST['guildFields'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/guild/".$realm."/".$guildName."?fields=".$guildFields."&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function guildMembers_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['realm']) && isset($_POST['guildName']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$realm = $_POST['realm'];
			$guildName = $_POST['guildName'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/guild/".$realm."/".$guildName."?fields=members&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function guildAchievements_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['realm']) && isset($_POST['guildName']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$realm = $_POST['realm'];
			$guildName = $_POST['guildName'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/guild/".$realm."/".$guildName."?fields=achievements&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function guildNews_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['realm']) && isset($_POST['guildName']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$realm = $_POST['realm'];
			$guildName = $_POST['guildName'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/guild/".$realm."/".$guildName."?fields=news&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function guildChallenge_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['realm']) && isset($_POST['guildName']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$realm = $_POST['realm'];
			$guildName = $_POST['guildName'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/guild/".$realm."/".$guildName."?fields=challenge&locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function pvpLeaderboards_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['bracket']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$bracket = $_POST['bracket'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/leaderboard/".$bracket."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function quest_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['questId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$questId = $_POST['questId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/quest/".$questId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function realmStatus_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/realm/status?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function recipe_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['recipeId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$recipeId = $_POST['recipeId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/recipe/".$recipeId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
		else
		{
			return response() -> json(["error" => "Parameters are off. Please check them!"]);
		}
	}
	
	public function spell_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['spellId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$spellId = $_POST['spellId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/spell/".$spellId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function zoneMasterList_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/zone/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function zone_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['zoneid']) && $_POST['locale'] && isset($_POST['jsonp']))
		{
			$zoneId = $_POST['zoneid'];
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/zone/".$zoneId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function battleGroups_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/battlegroups/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function characterRacesResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/character/races?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function characterClassesResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/character/classes?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function characterAchievementsResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/character/achievements?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function guildRewardsResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/guild/rewards?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function guildPerksResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/guild/perks?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function guildAchievementsResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/guild/achievements?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function itemClassesResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/item/classes?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function talentsResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/talents?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function petTypesResources_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/wow/data/pet/types?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	// Sc2 Data
	public function sc2Profiles_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['id']) && isset($_POST['name']) && isset($_POST['region']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$id = $_POST['id'];
			$region = $_POST['region'];
			$name = $_POST['name'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/sc2/profile/".$id."/".$region."/".$name."/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function sc2Ladders_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['id']) && isset($_POST['name']) && isset($_POST['region']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$id = $_POST['id'];
			$region = $_POST['region'];
			$name = $_POST['name'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/sc2/profile/".$id."/".$region."/".$name."/ladders?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function sc2MatchHistory_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['id']) && isset($_POST['name']) && isset($_POST['region']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$id = $_POST['id'];
			$region = $_POST['region'];
			$name = $_POST['name'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/sc2/profile/".$id."/".$region."/".$name."/matches?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function sc2Ladder_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['id']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$id = $_POST['id'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/sc2/ladder/".$id."/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function sc2AchievementsData_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/sc2/data/achievements?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function sc2RewardsData_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/sc2/data/rewards?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	// Diablo3 Data
	public function diablo3CareerProfiles_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['battleTag']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$battleTag = $_POST['battleTag'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/d3/profile/".$battleTag."/?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function diablo3HeroProfiles_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['battleTag']) && isset($_POST['heroId']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$battleTag = $_POST['battleTag'];
			$heroId = $_POST['heroId'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/d3/profile/".$battleTag."/hero/".$heroId."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function diablo3Items_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['itemData']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$itemData = $_POST['itemData'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/d3/data/item/".$itemData."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function diablo3Follower_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['followerData']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$followerData = $_POST['followerData'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/d3/data/follower/".$followerData."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
	
	public function diablo3Artisan_ctrl(Request $request)
    {
		$CustomProvider = new CustomProvider;
		$apiKey = $CustomProvider->initApiKey();
		
		if(isset($_POST['locale']) && isset($_POST['jsonp']) && isset($_POST['artisanData']))
		{
			$locale = $_POST['locale'];
			$jsonp = $_POST['jsonp'];
			$artisanData = $_POST['artisanData'];
			
			$ch = curl_init();
			$options = array(
				CURLOPT_URL => "https://us.api.battle.net/d3/data/artisan/".$artisanData."?locale=".$locale."&jsonp=".$jsonp."&apikey=" .$apiKey. "",
				CURLOPT_HEADER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_RETURNTRANSFER => true
			);

			curl_setopt_array($ch, $options);
			$content = curl_exec($ch);
			curl_close($ch);
			
			return response($content);
		}
	}
}