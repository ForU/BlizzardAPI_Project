<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about', function () {
    return 'Put about info here';
});

Route::get('contact', function () {
    return 'Put contact info here';
});

Route::delete('foo/bar', function () {
    //
});

Route::get('blade', ['middleware' => ['auth', 'old'], function () {
    //
	return view('layouts/master');
}]);

Route::get('test', function () {
    return view('layouts/test');
});

Route::get('oAuth/sc2Profile', 'CommunityOAuthProfile@sc2Profile');
Route::get('oAuth/wowProfile', 'CommunityOAuthProfile@wowProfile');
Route::get('oAuth/acctInfo', 'CommunityOAuthProfile@acctInfo');
Route::get('achievements', 'WoWData@achievements');
Route::get('auction', 'WoWData@auction');
Route::get('petMasterList', 'WoWData@petMasterList');
Route::get('battlePetAbility', 'WoWData@battlePetAbility');
Route::get('battlePetSpecies', 'WoWData@battlePetSpecies');
Route::get('wowChallengeRealm', 'WoWData@wowChallengeRealm');
Route::get('wowChallengeRegion', 'WoWData@wowChallengeRegion');
Route::get('wowCharacterProfile', 'WoWData@wowCharacterProfile');
Route::get('wowCharacterAchievements', 'WoWData@wowCharacterAchievements');
Route::get('wowCharacterAppearance', 'WoWData@wowCharacterAppearance');
Route::get('wowCharacterFeed', 'WoWData@wowCharacterFeed');
Route::get('wowCharacterGuild', 'WoWData@wowCharacterGuild');
Route::get('wowCharacterHunterPets', 'WoWData@wowCharacterHunterPets');
Route::get('wowCharacterItems', 'WoWData@wowCharacterItems');
Route::get('wowCharacterMounts', 'WoWData@wowCharacterMounts');
Route::get('wowCharacterPets', 'WoWData@wowCharacterPets');
Route::get('wowCharacterPetSlots', 'WoWData@wowCharacterPetSlots');
Route::get('wowCharacterProgression', 'WoWData@wowCharacterProgression');
Route::get('wowCharacterPvp', 'WoWData@wowCharacterPvp');
Route::get('wowCharacterQuests', 'WoWData@wowCharacterQuests');
Route::get('wowCharacterReputation', 'WoWData@wowCharacterReputation');
Route::get('wowCharacterStatistics', 'WoWData@wowCharacterStatistics');
Route::get('wowCharacterStats', 'WoWData@wowCharacterStats');
Route::get('wowCharacterTalents', 'WoWData@wowCharacterTalents');
Route::get('wowCharacterTitles', 'WoWData@wowCharacterTitles');
Route::get('wowCharacterAudit', 'WoWData@wowCharacterAudit');
Route::get('wowItemId', 'WoWData@wowItemId');
Route::get('wowItemSetId', 'WoWData@wowItemSetId');
Route::get('wowGuildProfile', 'WoWData@wowGuildProfile');
Route::get('wowGuildMembers', 'WoWData@wowGuildMembers');
Route::get('wowGuildAchievements', 'WoWData@wowGuildAchievements');
Route::get('wowGuildNews', 'WoWData@wowGuildNews');
Route::get('wowGuildChallenge', 'WoWData@wowGuildChallenge');
Route::get('wowPVPLeaderboards', 'WoWData@wowPVPLeaderboards');
Route::get('wowQuestId', 'WoWData@wowQuestId');
Route::get('wowRealmStatus', 'WoWData@wowRealmStatus');
Route::get('wowRecipeId', 'WoWData@wowRecipeId');
Route::get('wowSpellId', 'WoWData@wowSpellId');
Route::get('wowDataBattleGroups', 'WoWData@wowDataBattleGroups');
Route::get('wowDataCharacterRaces', 'WoWData@wowDataCharacterRaces');
Route::get('wowDataCharacterClasses', 'WoWData@wowDataCharacterClasses');
Route::get('wowDataCharacterAchievements', 'WoWData@wowDataCharacterAchievements');
Route::get('wowDataGuildRewards', 'WoWData@wowDataGuildRewards');
Route::get('wowDataGuildPerks', 'WoWData@wowDataGuildPerks');
Route::get('wowDataGuildAchievements', 'WoWData@wowDataGuildAchievements');
Route::get('wowDataItemClasses', 'WoWData@wowDataItemClasses');
Route::get('wowDataTalents', 'WoWData@wowDataTalents');
Route::get('wowDataPetTypes', 'WoWData@wowDataPetTypes');

/*** General Data Views ***/
Route::get('apiList_view', 'GeneralData@apiList_view');

/*** WowData Views ***/
Route::get('achievement_view', 'WoWData@achievement_view');
Route::get('auction_view', 'WoWData@auction_view');
Route::get('bossMasterList_view', 'WoWData@bossMasterList_view');
Route::get('bossInfo_view', 'WoWData@bossInfo_view');
Route::get('petMasterList_view', 'WoWData@petMasterList_view');
Route::get('petAbility_view', 'WoWData@petAbility_view');
Route::get('petSpecies_view', 'WoWData@petSpecies_view');
Route::get('petStats_view', 'WoWData@petStats_view');
Route::get('mountList_view', 'WoWData@mountList_view');
Route::get('challengeRealm_view', 'WoWData@challengeRealm_view');
Route::get('challengeRegion_view', 'WoWData@challengeRegion_view');
Route::get('characterProfile_view', 'WoWData@characterProfile_view');
Route::get('characterAchievements_view', 'WoWData@characterAchievements_view');
Route::get('characterAppearance_view', 'WoWData@characterAppearance_view');
Route::get('characterFeed_view', 'WoWData@characterFeed_view');
Route::get('characterGuild_view', 'WoWData@characterGuild_view');
Route::get('characterHunterPets_view', 'WoWData@characterHunterPets_view');
Route::get('characterItems_view', 'WoWData@characterItems_view');
Route::get('characterMounts_view', 'WoWData@characterMounts_view');
Route::get('characterPets_view', 'WoWData@characterPets_view');
Route::get('characterPetSlots_view', 'WoWData@characterPetSlots_view');
Route::get('characterProfessions_view', 'WoWData@characterProfessions_view');
Route::get('characterProgression_view', 'WoWData@characterProgression_view');
Route::get('characterPVP_view', 'WoWData@characterPVP_view');
Route::get('characterQuests_view', 'WoWData@characterQuests_view');
Route::get('characterReputation_view', 'WoWData@characterReputation_view');
Route::get('characterStatistics_view', 'WoWData@characterStatistics_view');
Route::get('characterStats_view', 'WoWData@characterStats_view');
Route::get('characterTalents_view', 'WoWData@characterTalents_view');
Route::get('characterTitles_view', 'WoWData@characterTitles_view');
Route::get('characterAudit_view', 'WoWData@characterAudit_view');
Route::get('itemId_view', 'WoWData@itemId_view');
Route::get('itemSetId_view', 'WoWData@itemSetId_view');
Route::get('guildProfile_view', 'WoWData@guildProfile_view');
Route::get('guildMembers_view', 'WoWData@guildMembers_view');
Route::get('guildAchievements_view', 'WoWData@guildAchievements_view');
Route::get('guildNews_view', 'WoWData@guildNews_view');
Route::get('guildChallenge_view', 'WoWData@guildChallenge_view');
Route::get('pvpLeaderboards_view', 'WoWData@pvpLeaderboards_view');
Route::get('quest_view', 'WoWData@quest_view');
Route::get('realmStatus_view', 'WoWData@realmStatus_view');
Route::get('recipe_view', 'WoWData@recipe_view');
Route::get('spell_view', 'WoWData@spell_view');
Route::get('zoneMasterList_view', 'WoWData@zoneMasterList_view');
Route::get('zone_view', 'WoWData@zone_view');
Route::get('battleGroups_view', 'WoWData@battleGroups_view');
Route::get('characterRacesResources_view', 'WoWData@characterRacesResources_view');
Route::get('characterClassesResources_view', 'WoWData@characterClassesResources_view');
Route::get('characterAchievementsResources_view', 'WoWData@characterAchievementsResources_view');
Route::get('guildRewardsResources_view', 'WoWData@guildRewardsResources_view');
Route::get('guildPerksResources_view', 'WoWData@guildPerksResources_view');
Route::get('guildAchievementsResources_view', 'WoWData@guildAchievementsResources_view');
Route::get('itemClassesResources_view', 'WoWData@itemClassesResources_view');
Route::get('talentsResources_view', 'WoWData@talentsResources_view');
Route::get('petTypesResources_view', 'WoWData@petTypesResources_view');

// SC2 Views
Route::get('sc2Profiles_view', 'SC2Data@sc2Profiles_view');
Route::get('sc2Ladders_view', 'SC2Data@sc2Ladders_view');
Route::get('sc2MatchHistory_view', 'SC2Data@sc2MatchHistory_view');
Route::get('sc2Ladder_view', 'SC2Data@sc2Ladder_view');
Route::get('sc2AchievementsData_view', 'SC2Data@sc2AchievementsData_view');
Route::get('sc2RewardsData_view', 'SC2Data@sc2RewardsData_view');

// Diablo3 Views
Route::get('diablo3CareerProfiles_view', 'Diablo3Data@diablo3CareerProfiles_view');
Route::get('diablo3HeroProfiles_view', 'Diablo3Data@diablo3HeroProfiles_view');
Route::get('diablo3Items_view', 'Diablo3Data@diablo3Items_view');
Route::get('diablo3Follower_view', 'Diablo3Data@diablo3Follower_view');
Route::get('diablo3Artisan_view', 'Diablo3Data@diablo3Artisan_view');
/*** !WowData Views! ***/

/*** ajaxFunction Controllers ***/
// Route::get('achievement_ctrl', 'ajaxFunctions@achievement_ctrl');
Route::post('achievement_ctrl', 'ajaxFunctions@achievement_ctrl');
Route::post('auction_ctrl', 'ajaxFunctions@auction_ctrl');
Route::post('bossMasterList_ctrl', 'ajaxFunctions@bossMasterList_ctrl');
Route::post('bossInfo_ctrl', 'ajaxFunctions@bossInfo_ctrl');
Route::post('petMasterList_ctrl', 'ajaxFunctions@petMasterList_ctrl');
Route::post('petAbility_ctrl', 'ajaxFunctions@petAbility_ctrl');
Route::post('petSpecies_ctrl', 'ajaxFunctions@petSpecies_ctrl');
Route::post('petStats_ctrl', 'ajaxFunctions@petStats_ctrl');
Route::post('mountList_ctrl', 'ajaxFunctions@mountList_ctrl');
Route::post('challengeRealm_ctrl', 'ajaxFunctions@challengeRealm_ctrl');
Route::post('challengeRegion_ctrl', 'ajaxFunctions@challengeRegion_ctrl');
Route::post('characterProfile_ctrl', 'ajaxFunctions@characterProfile_ctrl');
Route::post('characterAchievements_ctrl', 'ajaxFunctions@characterAchievements_ctrl');
Route::post('characterAppearance_ctrl', 'ajaxFunctions@characterAppearance_ctrl');
Route::post('characterFeed_ctrl', 'ajaxFunctions@characterFeed_ctrl');
Route::post('characterGuild_ctrl', 'ajaxFunctions@characterGuild_ctrl');
Route::post('characterHunterPets_ctrl', 'ajaxFunctions@characterHunterPets_ctrl');
Route::post('characterItems_ctrl', 'ajaxFunctions@characterItems_ctrl');
Route::post('characterMounts_ctrl', 'ajaxFunctions@characterMounts_ctrl');
Route::post('characterPets_ctrl', 'ajaxFunctions@characterPets_ctrl');
Route::post('characterPetSlots_ctrl', 'ajaxFunctions@characterPetSlots_ctrl');
Route::post('characterProfessions_ctrl', 'ajaxFunctions@characterProfessions_ctrl');
Route::post('characterProgression_ctrl', 'ajaxFunctions@characterProgression_ctrl');
Route::post('characterPVP_ctrl', 'ajaxFunctions@characterPVP_ctrl');
Route::post('characterQuests_ctrl', 'ajaxFunctions@characterQuests_ctrl');
Route::post('characterReputation_ctrl', 'ajaxFunctions@characterReputation_ctrl');
Route::post('characterStatistics_ctrl', 'ajaxFunctions@characterStatistics_ctrl');
Route::post('characterStats_ctrl', 'ajaxFunctions@characterStats_ctrl');
Route::post('characterTalents_ctrl', 'ajaxFunctions@characterTalents_ctrl');
Route::post('characterTitles_ctrl', 'ajaxFunctions@characterTitles_ctrl');
Route::post('characterAudit_ctrl', 'ajaxFunctions@characterAudit_ctrl');
Route::post('itemId_ctrl', 'ajaxFunctions@itemId_ctrl');
Route::post('itemSetId_ctrl', 'ajaxFunctions@itemSetId_ctrl');
Route::post('guildProfile_ctrl', 'ajaxFunctions@guildProfile_ctrl');
Route::post('guildMembers_ctrl', 'ajaxFunctions@guildMembers_ctrl');
Route::post('guildAchievements_ctrl', 'ajaxFunctions@guildAchievements_ctrl');
Route::post('guildNews_ctrl', 'ajaxFunctions@guildNews_ctrl');
Route::post('guildChallenge_ctrl', 'ajaxFunctions@guildChallenge_ctrl');
Route::post('pvpLeaderboards_ctrl', 'ajaxFunctions@pvpLeaderboards_ctrl');
Route::post('quest_ctrl', 'ajaxFunctions@quest_ctrl');
Route::post('realmStatus_ctrl', 'ajaxFunctions@realmStatus_ctrl');
Route::post('recipe_ctrl', 'ajaxFunctions@recipe_ctrl');
Route::post('spell_ctrl', 'ajaxFunctions@spell_ctrl');
Route::post('zoneMasterList_ctrl', 'ajaxFunctions@zoneMasterList_ctrl');
Route::post('zone_ctrl', 'ajaxFunctions@zone_ctrl');
Route::post('battleGroups_ctrl', 'ajaxFunctions@battleGroups_ctrl');
Route::post('characterRacesResources_ctrl', 'ajaxFunctions@characterRacesResources_ctrl');
Route::post('characterClassesResources_ctrl', 'ajaxFunctions@characterClassesResources_ctrl');
Route::post('characterAchievementsResources_ctrl', 'ajaxFunctions@characterAchievementsResources_ctrl');
Route::post('guildRewardsResources_ctrl', 'ajaxFunctions@guildRewardsResources_ctrl');
Route::post('guildPerksResources_ctrl', 'ajaxFunctions@guildPerksResources_ctrl');
Route::post('guildAchievementsResources_ctrl', 'ajaxFunctions@guildAchievementsResources_ctrl');
Route::post('itemClassesResources_ctrl', 'ajaxFunctions@itemClassesResources_ctrl');
Route::post('talentsResources_ctrl', 'ajaxFunctions@talentsResources_ctrl');
Route::post('petTypesResources_ctrl', 'ajaxFunctions@petTypesResources_ctrl');

// SC2 Controllers
Route::post('sc2Profiles_ctrl', 'ajaxFunctions@sc2Profiles_ctrl');
Route::post('sc2Ladders_ctrl', 'ajaxFunctions@sc2Ladders_ctrl');
Route::post('sc2MatchHistory_ctrl', 'ajaxFunctions@sc2MatchHistory_ctrl');
Route::post('sc2Ladder_ctrl', 'ajaxFunctions@sc2Ladder_ctrl');
Route::post('sc2AchievementsData_ctrl', 'ajaxFunctions@sc2AchievementsData_ctrl');
Route::post('sc2RewardsData_ctrl', 'ajaxFunctions@sc2RewardsData_ctrl');

// Diablo3 Controllers
Route::post('diablo3CareerProfiles_ctrl', 'ajaxFunctions@diablo3CareerProfiles_ctrl');
Route::post('diablo3HeroProfiles_ctrl', 'ajaxFunctions@diablo3HeroProfiles_ctrl');
Route::post('diablo3Items_ctrl', 'ajaxFunctions@diablo3Items_ctrl');
Route::post('diablo3Follower_ctrl', 'ajaxFunctions@diablo3Follower_ctrl');
Route::post('diablo3Artisan_ctrl', 'ajaxFunctions@diablo3Artisan_ctrl');
/*** !ajaxFunction Controllers! ***/