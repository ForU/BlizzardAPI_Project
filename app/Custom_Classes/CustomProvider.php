<?php

namespace App\Custom_Classes;

class CustomProvider
{
	public static function initSc2Provider()
	{
		$provider = new \League\OAuth2\Client\Provider\GenericProvider([
			'clientId'                => 'dt6yax43dewdvafjdpxwtcdz5m6pb58j',    // The client ID assigned to you by the provider
			'clientSecret'            => 'TMAKt4aCvs6MfRVyERdDQ5PTKcps3DnM',   // The client password assigned to you by the provider
			// 'redirectUri'             => 'https://hearthstorm.com/oAuth/sc2Profile',
			'redirectUri'             => 'https://hearthlode.com/oAuth/sc2Profile',
			'urlAuthorize'            => 'https://us.battle.net/oauth/authorize',
			'urlAccessToken'          => 'https://us.battle.net/oauth/token',
			'urlResourceOwnerDetails' => 'https://us.api.battle.net/sc2/profile/user',
			'scopes'				  => 'sc2.profile'
		]);
		return $provider;
	}
	
	public static function initWoWProvider()
	{
		$provider = new \League\OAuth2\Client\Provider\GenericProvider([
			'clientId'                => '5grykrssk3qvwurcdy96spdrkpgr2xqp',    // The client ID assigned to you by the provider
			'clientSecret'            => 'uVW3gxsVtzEC4jYRKhj3WHm5zSevGqx4',   // The client password assigned to you by the provider
			// 'redirectUri'             => 'https://hearthstorm.com/oAuth/wowProfile',
			'redirectUri'             => 'https://hearthlode.com/oAuth/wowProfile',
			'urlAuthorize'            => 'https://us.battle.net/oauth/authorize',
			'urlAccessToken'          => 'https://us.battle.net/oauth/token',
			'urlResourceOwnerDetails' => 'https://us.api.battle.net/wow/user/characters',
			'scopes'				  => 'wow.profile'
		]);
		return $provider;
	}
	
	public static function initAccountProvider()
	{
		$provider = new \League\OAuth2\Client\Provider\GenericProvider([
			'clientId'                => '6rryvnvjext4xyh87q7tauytzxchbyht',    // The client ID assigned to you by the provider
			'clientSecret'            => 'USDZWQJgU7XpK6AsTjxGa4DHpQHCXZPv',   // The client password assigned to you by the provider
			// 'redirectUri'             => 'https://hearthstorm.com/oAuth/acctInfo',
			'redirectUri'             => 'https://hearthlode.com/oAuth/acctInfo',
			'urlAuthorize'            => 'https://us.battle.net/oauth/authorize',
			'urlAccessToken'          => 'https://us.battle.net/oauth/token',
			'urlResourceOwnerDetails' => 'https://us.api.battle.net/account/user'
		]);
		return $provider;
	}
	
	public static function initApiKey()
	{
		$apiKey = '6rryvnvjext4xyh87q7tauytzxchbyht';
		return $apiKey;
	}
}