var apiCategory_opt = {
	'': '',
	'WoW Community API (US)': 'wow_us_api',
	'D3 Community API (US)': 'd3_us_api',
	'SC2 Community API (US)': 'sc2_us_api',
};

$.each(apiCategory_opt, function(key_opt, value_opt) {
		$("#apiCategory").append($('<option>', {
			value: value_opt,
			text: key_opt
		}));
});

// setInterval(function() {
	$("#apiCategory").on("change", function() {
		if($("#apiCategory").val() == "")
		{
			$("#gameApi").html("");
		}
		else if ($("#apiCategory").val() == "wow_us_api")
		{
			console.log($("#apiCategory").val());
			$("#gameApi").html("");
			$("#gameApi").append('<label class="labels">WoW API List: </label>');
			// $("#gameApi").append('<select id="wowApi" class="form-control"></select>');
			$("<select />", {
				id: "wowApi",
				class: "form-control"
			}).appendTo($("#gameApi")).trigger("appended", ["wowApiName"]);
		}
		else if ($("#apiCategory").val() == "sc2_us_api")
		{
			console.log($("#apiCategory").val());
			$("#gameApi").html("");
			$("#gameApi").append('<label class="labels">SC2 API List: </label>');
			// $("#gameApi").append('<select id="sc2Api" class="form-control"></select>');
			$("<select />", {
				id: "sc2Api",
				class: "form-control"
			}).appendTo($("#gameApi")).trigger("appended", ["sc2ApiName"]);
		}
		else if ($("#apiCategory").val() == "d3_us_api")
		{
			console.log($("#apiCategory").val());
			$("#gameApi").html("");
			$("#gameApi").append('<label class="labels">D3 API List: </label>');
			// $("#gameApi").append('<select id="sc2Api" class="form-control"></select>');
			$("<select />", {
				id: "d3Api",
				class: "form-control"
			}).appendTo($("#gameApi")).trigger("appended", ["d3ApiName"]);
		}
		else
		{}
	});
// }, 1000);

var wowApi_US_opt = {
	'Achievement API': 'achievement_api',
	'Auction API': 'auction_api',
	'Boss Master List API': 'bossML_api',
	'Boss Info API': 'bossInfo_api',
	'Challenge Realm Leaderboard API': 'chaRealm_api',
	'Challenge Region Leaderboard API': 'chaRegion_api',
	'Character Profile API': 'characterProf_api',
	'Character Achievements API': 'characterAch_api',
	'Character Appearance API': 'characterAppear_api',
	'Character Feed API': 'characterFeed_api',
	'Character Guild API': 'characterGuild_api',
	'Character Hunter Pets API': 'characterHunterPets_api',
	'Character Items API': 'characterItems_api',
	'Character Mounts API': 'characterMounts_api',
	'Character Pets API': 'characterPets_api',
	'Character Pet Slots API': 'characterPetSlots_api',
	'Character Professions API': 'characterProfessions_api',
	'Character PVP API': 'characterPVP_api',
	'Character Quests API': 'characterQuests_api',
	'Character Reputation API': 'characterReputation_api',
	'Character Statistics API': 'characterStatistics_api',
	'Character Stats API': 'characterStats_api',
	'Character Talents API': 'characterTalents_api',
	'Character Titles API': 'characterTitles_api',
	'Character Titles Audit': 'characterAudit_api',
	'Guild Profile API': 'guildProfile_api',
	'Guild Members API': 'guildMembers_api',
	'Guild Achievements API': 'guildAchievements_api',
	'Guild News API': 'guildNews_api',
	'Guild Challenge API': 'guildChallenge_api',
	'Item API': 'item_api',
	'Item Set API': 'itemSet_api',
	'Mount Master List API': 'mountMasterList_api',
	'Pet Master List API': 'petMasterList_api',
	'Pet Abilities API': 'petAbility_api',
	'Pet Species API': 'petSpecies_api',
	'Pet Stats API': 'petStats_api',
	'PVP LeaderBoards API': 'pvpLeaderboards_api',
	'Quest API': 'quest_api',
	'Realm Status API': 'realmStatus_api',
	'Recipe API': 'recipe_api',
	'Spell API': 'spell_api',
	'Zone Master List API': 'zoneMasterList_api',
	'Zone API': 'zone_api',
	'Battlegroups API': 'battleGroups_api',
	'Character Races Resources API': 'characterRacesResources_api',
	'Character Classes Resources API': 'characterClassesResources_api',
	'Character Achievements Resources API': 'characterAchievementsResources_api',
	'Guild Rewards Resources API': 'guildRewardsResources_api',
	'Guild Perks Resources API': 'guildPerksResources_api',
	'Guild Achievements Resources API': 'guildAchievementsResources_api',
	'Item Classes Resources API': 'itemClassesResources_api',
	'Talents Resources API': 'talentsResources_api',
	'Pet Types Resources API': 'petTypesResources_api',
};

var sc2Api_US_opt = {
	'Profile API': 'sc2Profile_api',
	'Ladders API': 'sc2Ladders_api',
	'Match History API': 'sc2MatchHistory_api',
	'Ladder API': 'sc2Ladder_api',
	'Achievements API': 'sc2AchievementsData_api',
	'Rewards API': 'sc2RewardsData_api',
};

var d3Api_US_opt = {
	'Career Profile API': 'd3CareerProfile_api',
	'Hero Profile API': 'd3HeroProfile_api',
	'Items Data API': 'd3ItemsData_api',
	'Follower Data API': 'd3FollowerData_api',
	'Artisan Data API': 'd3ArtisanData_api',
};

// setInterval(function() {
// $.each(wowApi_US_opt, function(key_opt, value_opt) {
		// $("#wowApi").append($('<option>', {
			// value: value_opt,
			// text: key_opt
		// }));
// });
// }, 1000);

// $("body").on("appended", "select", function(event)
$("body").on("appended", function(event, apiName)
{
	$.each(wowApi_US_opt, function(key_opt, value_opt) {
		$("#wowApi").append($('<option>', {
			value: value_opt,
			text: key_opt
		}));
	});
	
	$.each(sc2Api_US_opt, function(key_opt, value_opt) {
		$("#sc2Api").append($('<option>', {
			value: value_opt,
			text: key_opt
		}));
	});
	
	$.each(d3Api_US_opt, function(key_opt, value_opt) {
		$("#d3Api").append($('<option>', {
			value: value_opt,
			text: key_opt
		}));
	});
});

var locale_opt = {
	'en_US': 'en_US',
	'pt_BR': 'pt_BR',
	'es_MX': 'es_MX',
};

$.each(locale_opt, function(key_opt, value_opt) {
		$("#locale").append($('<option>', {
			value: value_opt,
			text: value_opt
		}));
});

var jsonp_opt = {
	'': '',
	'jsonp': 'jsonp',
};

$.each(jsonp_opt, function(key_opt, value_opt) {
		$("#jsonp").append($('<option>', {
			value: value_opt,
			text: value_opt
		}));
});

var profileFields_opt = {
	'': '',
	'achievements': 'achievements',
	'feed': 'feed',
};

$.each(profileFields_opt, function(key_opt, value_opt) {
		$("#profileFields").append($('<option>', {
			value: value_opt,
			text: value_opt
		}));
});

var guildFields_opt = {
	'achievements': 'achievements',
	'challenge': 'challenge'
};

$.each(guildFields_opt, function(key_opt, value_opt) {
		$("#guildFields").append($('<option>', {
			value: value_opt,
			text: value_opt
		}));
});

var bracket_opt = {
	'2v2': '2v2',
	'3v3': '3v3',
	'5v5': '5v5',
	'rbg': 'rbg',
};

$.each(bracket_opt, function(key_opt, value_opt) {
		$("#bracket").append($('<option>', {
			value: value_opt,
			text: value_opt
		}));
});

/** Disqus Code Section **/
	$(document.body).on('click', '.show-comments', function(){
		  var disqus_shortname = 'hearthlode'; // Replace this value with *your* username.

		if (typeof(disqus_loaded) == "undefined" || !disqus_loaded) {
			  disqus_loaded = 1;
			  $.ajax({
					  type: "GET",
					  url: "//" + disqus_shortname + ".disqus.com/embed.js",
					  dataType: "script",
					  cache: true
			  });
		}
		  $(this).html('Hide Comments');
		  $("#disqus_thread").show();
		  $(this).removeClass('show-comments').addClass('hide-comments');
	});
	
	$(document.body).on('click', '.hide-comments', function()
	{
		  $(this).html('Show Comments');
		  $(this).removeClass('hide-comments').addClass('show-comments');
		  $("#disqus_thread").hide();
	});
/** !Disqus Code Section **/

/*** Placeholder functions ***/
if (document.getElementById("realm") != null)
{
	document.getElementById("realm").placeholder = "fenris";
}
$("#realm").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("realm").placeholder = "fenris";
	}
});

if (document.getElementById("characterName") != null)
{
	document.getElementById("characterName").placeholder = "john";
}
$("#characterName").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("characterName").placeholder = "john";
	}
});

if (document.getElementById("id") != null)
{
	document.getElementById("id").placeholder = "2144";
}
$("#id").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("id").placeholder = "2144";
	}
});

if (document.getElementById("gameid") != null)
{
	document.getElementById("gameid").placeholder = "440";
}
$("#gameid").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("gameid").placeholder = "440";
	}
});

if (document.getElementById("battleTag") != null)
{
	document.getElementById("battleTag").placeholder = "Fenris-1855";
}
$("#battleTag").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("battleTag").placeholder = "Fenris-1855";
	}
});

if (document.getElementById("heroId") != null)
{
	document.getElementById("heroId").placeholder = "1";
}
$("#heroId").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("heroId").placeholder = "1";
	}
});

if (document.getElementById("guildName") != null)
{
	document.getElementById("guildName").placeholder = "Lords Of Darkness";
}
$("#guildName").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("guildName").placeholder = "Lords of Darkness";
	}
});

if (document.getElementById("itemId") != null)
{
	document.getElementById("itemId").placeholder = "22279";
}
$("#itemId").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("itemId").placeholder = "22279";
	}
});

if (document.getElementById("itemSetId") != null)
{
	document.getElementById("itemSetId").placeholder = "668";
}
$("#itemSetId").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("itemSetId").placeholder = "668";
	}
});

if (document.getElementById("abilityId") != null)
{
	document.getElementById("abilityId").placeholder = "240";
}
$("#abilityId").focus(function ()
{
	$(this).removeAttr('placeholder');
	if($(this).val().length === 0)
	{
		document.getElementById("abilityId").placeholder = "240";
	}
});
/*** !Placeholder functions! ***/

$("#apiList-form").submit(function(e)
{
	console.log($("#wowApi").val());
	if($("#wowApi").val() == "") {}
	else if($("#wowApi").val() == "achievement_api")
		window.location.href = "https://hearthlode.com/achievement_view";
	else if($("#wowApi").val() == "auction_api")
		window.location.href = "https://hearthlode.com/auction_view";
	else if($("#wowApi").val() == "bossML_api")
		window.location.href = "https://hearthlode.com/bossMasterList_view";
	else if($("#wowApi").val() == "bossInfo_api")
		window.location.href = "https://hearthlode.com/bossInfo_view";
	else if($("#wowApi").val() == "chaRealm_api")
		window.location.href = "https://hearthlode.com/challengeRealm_view";
	else if($("#wowApi").val() == "chaRegion_api")
		window.location.href = "https://hearthlode.com/challengeRegion_view";
	else if($("#wowApi").val() == "characterProf_api")
		window.location.href = "https://hearthlode.com/characterProfile_view";
	else if($("#wowApi").val() == "characterAch_api")
		window.location.href = "https://hearthlode.com/characterAchievements_view";
	else if($("#wowApi").val() == "characterAppear_api")
		window.location.href = "https://hearthlode.com/characterAppearance_view";
	else if($("#wowApi").val() == "characterFeed_api")
		window.location.href = "https://hearthlode.com/characterFeed_view";
	else if($("#wowApi").val() == "characterGuild_api")
		window.location.href = "https://hearthlode.com/characterGuild_view";
	else if($("#wowApi").val() == "characterHunterPets_api")
		window.location.href = "https://hearthlode.com/characterHunterPets_view";
	else if($("#wowApi").val() == "characterItems_api")
		window.location.href = "https://hearthlode.com/characterItems_view";
	else if($("#wowApi").val() == "characterMounts_api")
		window.location.href = "https://hearthlode.com/characterMounts_view";
	else if($("#wowApi").val() == "characterPets_api")
		window.location.href = "https://hearthlode.com/characterPets_view";
	else if($("#wowApi").val() == "characterPetSlots_api")
		window.location.href = "https://hearthlode.com/characterPetSlots_view";
	else if($("#wowApi").val() == "characterProfessions_api")
		window.location.href = "https://hearthlode.com/characterProfessions_view";
	else if($("#wowApi").val() == "characterPVP_api")
		window.location.href = "https://hearthlode.com/characterPVP_view";
	else if($("#wowApi").val() == "characterQuests_api")
		window.location.href = "https://hearthlode.com/characterQuests_view";
	else if($("#wowApi").val() == "characterReputation_api")
		window.location.href = "https://hearthlode.com/characterReputation_view";
	else if($("#wowApi").val() == "characterStatistics_api")
		window.location.href = "https://hearthlode.com/characterStatistics_view";
	else if($("#wowApi").val() == "characterStats_api")
		window.location.href = "https://hearthlode.com/characterStats_view";
	else if($("#wowApi").val() == "characterTalents_api")
		window.location.href = "https://hearthlode.com/characterTalents_view";
	else if($("#wowApi").val() == "characterTitles_api")
		window.location.href = "https://hearthlode.com/characterTitles_view";
	else if($("#wowApi").val() == "characterAudit_api")
		window.location.href = "https://hearthlode.com/characterAudit_view";
	else if($("#wowApi").val() == "guildProfile_api")
		window.location.href = "https://hearthlode.com/guildProfile_view";
	else if($("#wowApi").val() == "guildMembers_api")
		window.location.href = "https://hearthlode.com/guildMembers_view";
	else if($("#wowApi").val() == "guildAchievements_api")
		window.location.href = "https://hearthlode.com/guildAchievements_view";
	else if($("#wowApi").val() == "guildNews_api")
		window.location.href = "https://hearthlode.com/guildNews_view";
	else if($("#wowApi").val() == "guildChallenge_api")
		window.location.href = "https://hearthlode.com/guildChallenge_view";
	else if($("#wowApi").val() == "item_api")
		window.location.href = "https://hearthlode.com/itemId_view";
	else if($("#wowApi").val() == "itemSet_api")
		window.location.href = "https://hearthlode.com/itemSetId_view";
	else if($("#wowApi").val() == "mountMasterList_api")
		window.location.href = "https://hearthlode.com/mountList_view";
	else if($("#wowApi").val() == "petMasterList_api")
		window.location.href = "https://hearthlode.com/petMasterList_view";
	else if($("#wowApi").val() == "petAbility_api")
		window.location.href = "https://hearthlode.com/petAbility_view";
	else if($("#wowApi").val() == "petSpecies_api")
		window.location.href = "https://hearthlode.com/petSpecies_view";
	else if($("#wowApi").val() == "petStats_api")
		window.location.href = "https://hearthlode.com/petStats_view";
	else if($("#wowApi").val() == "pvpLeaderboards_api")
		window.location.href = "https://hearthlode.com/pvpLeaderboards_view";
	else if($("#wowApi").val() == "quest_api")
		window.location.href = "https://hearthlode.com/quest_view";
	else if($("#wowApi").val() == "realmStatus_api")
		window.location.href = "https://hearthlode.com/realmStatus_view";
	else if($("#wowApi").val() == "recipe_api")
		window.location.href = "https://hearthlode.com/recipe_view";
	else if($("#wowApi").val() == "spell_api")
		window.location.href = "https://hearthlode.com/spell_view";
	else if($("#wowApi").val() == "zoneMasterList_api")
		window.location.href = "https://hearthlode.com/zoneMasterList_view";
	else if($("#wowApi").val() == "zone_api")
		window.location.href = "https://hearthlode.com/zone_view";
	else if($("#wowApi").val() == "battleGroups_api")
		window.location.href = "https://hearthlode.com/battleGroups_view";
	else if($("#wowApi").val() == "characterRacesResources_api")
		window.location.href = "https://hearthlode.com/characterRacesResources_view";
	else if($("#wowApi").val() == "characterClassesResources_api")
		window.location.href = "https://hearthlode.com/characterClassesResources_view";
	else if($("#wowApi").val() == "characterAchievementsResources_api")
		window.location.href = "https://hearthlode.com/characterAchievementsResources_view";
	else if($("#wowApi").val() == "guildRewardsResources_api")
		window.location.href = "https://hearthlode.com/guildRewardsResources_view";
	else if($("#wowApi").val() == "guildPerksResources_api")
		window.location.href = "https://hearthlode.com/guildPerksResources_view";
	else if($("#wowApi").val() == "guildAchievementsResources_api")
		window.location.href = "https://hearthlode.com/guildAchievementsResources_view";
	else if($("#wowApi").val() == "itemClassesResources_api")
		window.location.href = "https://hearthlode.com/itemClassesResources_view";
	else if($("#wowApi").val() == "talentsResources_api")
		window.location.href = "https://hearthlode.com/talentsResources_view";
	else if($("#wowApi").val() == "petTypesResources_api")
		window.location.href = "https://hearthlode.com/petTypesResources_view";
	
	console.log($("#sc2Api").val());
	if($("#sc2Api").val() == "") {}
	else if($("#sc2Api").val() == "sc2Profile_api")
		window.location.href = "https://hearthlode.com/sc2Profiles_view";
	else if($("#sc2Api").val() == "sc2Ladders_api")
		window.location.href = "https://hearthlode.com/sc2Ladders_view";
	else if($("#sc2Api").val() == "sc2MatchHistory_api")
		window.location.href = "https://hearthlode.com/sc2MatchHistory_view";
	else if($("#sc2Api").val() == "sc2Ladder_api")
		window.location.href = "https://hearthlode.com/sc2Ladder_view";
	else if($("#sc2Api").val() == "sc2AchievementsData_api")
		window.location.href = "https://hearthlode.com/sc2AchievementsData_view";
	else if($("#sc2Api").val() == "sc2RewardsData_api")
		window.location.href = "https://hearthlode.com/sc2RewardsData_view";
	
	console.log($("#d3Api").val());
	if($("#d3Api").val() == "") {}
	else if($("#d3Api").val() == "d3CareerProfile_api")
		window.location.href = "https://hearthlode.com/diablo3CareerProfiles_view";
	else if($("#d3Api").val() == "d3HeroProfile_api")
		window.location.href = "https://hearthlode.com/diablo3HeroProfiles_view";
	else if($("#d3Api").val() == "d3ItemsData_api")
		window.location.href = "https://hearthlode.com/diablo3Items_view";
	else if($("#d3Api").val() == "d3FollowerData_api")
		window.location.href = "https://hearthlode.com/diablo3Follower_view";
	else if($("#d3Api").val() == "d3ArtisanData_api")
		window.location.href = "https://hearthlode.com/diablo3Artisan_view";
	
	return false;
});

$("#achievement-form").submit(function(e)
{
	// $.ajaxSetup({headers:{'csrftoken':'{{csrf_token()}}'}});
	$.ajax({
		// url:"/achievement_ctrl",
		url:"achievement_ctrl",
		// headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		// beforeSend:function(xhr)
		// {
			// var token = $("#token").val();
			// if(token)
			// {
				// return xhr.setRequestHeader('X-CSRF-TOKEN', token);
			// }
		// },
		method: "post",
		// dataType: "json",
		data: {id: $("#id").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		// console.log(msg[0]);
		// $.each(msg[0], function(key, value)
		// {
			// $(".db_table").append("<tr><td>"+key+"</td></tr>");
			// $(".db_table").append("<tr><td>"+value+"</td></tr>");
		// });
		// var jsonString = JSON.stringify(msg, null, '\t');
		// document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		// var json = $.parseJSON(msg);
		// document.getElementById("results").innerHTML = JSON.stringify(json, undefined, 4);
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	})
	.fail(function(jqXHR, textStatus, errorThrown)
	{
		document.getElementById("results").innerHTML = "<pre><code>"+errorThrown+"</code></pre>";
		console.log(jqXHR);
		console.log(textStatus);
		console.log(errorThrown);
	});
	return false;
});

$("#auction-form").submit(function(e)
{
	$.ajax({
		url:"auction_ctrl",
		method: "post",
		// dataType: "json",
		data: {realm: $("#realm").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		// var jsonString = JSON.stringify(msg, null, '\t');
		// document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	})
	.fail(function(jqXHR, textStatus, errorThrown)
	{
		document.getElementById("results").innerHTML = "<pre><code>"+errorThrown+"</code></pre>";
		console.log(jqXHR);
		console.log(textStatus);
		console.log(errorThrown);
	});
	return false;
});

$("#bossMasterList-form").submit(function(e)
{
	$.ajax({
		url:"bossMasterList_ctrl",
		method: "post",
		// dataType: "json",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	})
	.fail(function(jqXHR, textStatus, errorThrown)
	{
		document.getElementById("results").innerHTML = "<pre><code>"+errorThrown+"</code></pre>";
		console.log(jqXHR);
		console.log(textStatus);
		console.log(errorThrown);
	});
	return false;
});

$("#bossInfo-form").submit(function(e)
{
	$.ajax({
		url:"bossInfo_ctrl",
		method: "post",
		// dataType: "json",
		data: {bossId: $("#bossId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	})
	.fail(function(jqXHR, textStatus, errorThrown)
	{
		document.getElementById("results").innerHTML = "<pre><code>"+errorThrown+"</code></pre>";
		console.log(jqXHR);
		console.log(textStatus);
		console.log(errorThrown);
	});
	return false;
});

$("#petMasterList-form").submit(function(e)
{
	$.ajax({
		url:"petMasterList_ctrl",
		method: "post",
		// dataType: "json",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		// var jsonString = JSON.stringify(msg, null, '\t');
		// document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#petAbility-form").submit(function(e)
{
	$.ajax({
		url:"petAbility_ctrl",
		method: "post",
		// dataType: "json",
		data: {abilityId: $("#abilityId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		// var jsonString = JSON.stringify(msg, null, '\t');
		// document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#petSpecies-form").submit(function(e)
{
	$.ajax({
		url:"petSpecies_ctrl",
		method: "post",
		data: {speciesId: $("#speciesId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#petStats-form").submit(function(e)
{
	$.ajax({
		url:"petStats_ctrl",
		method: "post",
		data: {speciesId: $("#speciesId").val(), level: $("#level").val(), breedId: $("#breedId").val(), qualityId: $("#qualityId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#mountList-form").submit(function(e)
{
	$.ajax({
		url:"mountList_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#challengeRealm-form").submit(function(e)
{
	$.ajax({
		url:"challengeRealm_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#challengeRegion-form").submit(function(e)
{
	$.ajax({
		url:"challengeRegion_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterProfile-form").submit(function(e)
{
	$.ajax({
		url:"characterProfile_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), profileFields: $("#profileFields").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

// $("#characterProfile-form").submit(function(e)
// {
	// $.ajax({
		// url:"characterProfile_ctrl",
		// method: "post",
		// data: {realm: $("#realm").val(), characterName: $("#characterName").val(), profileFields: $("#profileFields").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	// }).done(function(msg) 
	// {
		// if(msg.hasOwnProperty('error'))
		// {
			// console.log(msg.error);
			// document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		// }
		
		// if($("#jsonp").val() === "")
		// {
			// jMsg = JSON.parse(msg);
			// var jsonString = JSON.stringify(jMsg, null, '\t');
			// document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		// }
		// else if($("#jsonp").val().length !== "")
		// {	
			// jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			// jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			// jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			// jMsg_conv = JSON.parse(jMsg);
			// var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			// document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		// }
		// else
		// {
			// document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		// }
	// });
	// return false;
// });

$("#characterAchievements-form").submit(function(e)
{
	$.ajax({
		url:"characterAchievements_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterAppearance-form").submit(function(e)
{
	$.ajax({
		url:"characterAppearance_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterFeed-form").submit(function(e)
{
	$.ajax({
		url:"characterFeed_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterGuild-form").submit(function(e)
{
	$.ajax({
		url:"characterGuild_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterHunterPets-form").submit(function(e)
{
	$.ajax({
		url:"characterHunterPets_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterItems-form").submit(function(e)
{
	$.ajax({
		url:"characterItems_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterMounts-form").submit(function(e)
{
	$.ajax({
		url:"characterMounts_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterPets-form").submit(function(e)
{
	$.ajax({
		url:"characterPets_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterPetSlots-form").submit(function(e)
{
	$.ajax({
		url:"characterPetSlots_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterProfessions-form").submit(function(e)
{
	$.ajax({
		url:"characterProfessions_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterProgression-form").submit(function(e)
{
	$.ajax({
		url:"characterProgression_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterPVP-form").submit(function(e)
{
	$.ajax({
		url:"characterPVP_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterQuests-form").submit(function(e)
{
	$.ajax({
		url:"characterQuests_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterReputation-form").submit(function(e)
{
	$.ajax({
		url:"characterReputation_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterStatistics-form").submit(function(e)
{
	$.ajax({
		url:"characterStatistics_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterStats-form").submit(function(e)
{
	$.ajax({
		url:"characterStats_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterTalents-form").submit(function(e)
{
	$.ajax({
		url:"characterTalents_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterTitles-form").submit(function(e)
{
	$.ajax({
		url:"characterTitles_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterAudit-form").submit(function(e)
{
	$.ajax({
		url:"characterAudit_ctrl",
		method: "post",
		data: {realm: $("#realm").val(), characterName: $("#characterName").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#itemId-form").submit(function(e)
{
	$.ajax({
		url:"itemId_ctrl",
		method: "post",
		data: {itemId: $("#itemId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#itemSetId-form").submit(function(e)
{
	$.ajax({
		url:"itemSetId_ctrl",
		method: "post",
		data: {itemSetId: $("#itemSetId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildProfile-form").submit(function(e)
{
	var foo = []; 
	var guildNameAlt = $("#guildName").val().replace(/ /g,"%20");
	$('#guildFields :selected').each(function(i, selected){ 
	  foo[i] = $(selected).text(); 
	});
	var fields = foo.join(",");
	
	$.ajax({
		url:"guildProfile_ctrl",
		method: "post",
		data: {guildName: guildNameAlt, realm: $("#realm").val(), guildFields: fields, locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildMembers-form").submit(function(e)
{	
	var guildNameAlt = $("#guildName").val().replace(/ /g,"%20");
	$.ajax({
		url:"guildMembers_ctrl",
		method: "post",
		data: {guildName: guildNameAlt, realm: $("#realm").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildAchievements-form").submit(function(e)
{	
	var guildNameAlt = $("#guildName").val().replace(/ /g,"%20");
	console.log(guildNameAlt);
	$.ajax({
		url:"guildAchievements_ctrl",
		method: "post",
		data: {guildName: guildNameAlt, realm: $("#realm").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildNews-form").submit(function(e)
{	
	var guildNameAlt = $("#guildName").val().replace(/ /g,"%20");
	$.ajax({
		url:"guildNews_ctrl",
		method: "post",
		data: {guildName: guildNameAlt, realm: $("#realm").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildChallenge-form").submit(function(e)
{	
	var guildNameAlt = $("#guildName").val().replace(/ /g,"%20");
	$.ajax({
		url:"guildChallenge_ctrl",
		method: "post",
		data: {guildName: guildNameAlt, realm: $("#realm").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#pvpLeaderboards-form").submit(function(e)
{	
	$.ajax({
		url:"pvpLeaderboards_ctrl",
		method: "post",
		data: {bracket: $("#bracket").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#quest-form").submit(function(e)
{	
	$.ajax({
		url:"quest_ctrl",
		method: "post",
		data: {questId: $("#questId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#realmStatus-form").submit(function(e)
{	
	$.ajax({
		url:"realmStatus_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#recipe-form").submit(function(e)
{	
	$.ajax({
		url:"recipe_ctrl",
		method: "post",
		data: {recipeId: $("#recipeId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if(msg.hasOwnProperty('error'))
		{
			console.log(msg.error);
			document.getElementById("results").innerHTML = "<pre><code>"+msg.error+"</code></pre>";
		}
		
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#spell-form").submit(function(e)
{	
	$.ajax({
		url:"spell_ctrl",
		method: "post",
		data: {spellId: $("#spellId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#zoneMasterList-form").submit(function(e)
{	
	$.ajax({
		url:"zoneMasterList_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#zone-form").submit(function(e)
{	
	$.ajax({
		url:"zone_ctrl",
		method: "post",
		data: {zoneId: $("#zoneId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#battleGroups-form").submit(function(e)
{	
	$.ajax({
		url:"battleGroups_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterRacesResources-form").submit(function(e)
{	
	$.ajax({
		url:"characterRacesResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterClassesResources-form").submit(function(e)
{	
	$.ajax({
		url:"characterClassesResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#characterAchievementsResources-form").submit(function(e)
{	
	$.ajax({
		url:"characterAchievementsResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildRewardsResources-form").submit(function(e)
{	
	$.ajax({
		url:"guildRewardsResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildPerksResources-form").submit(function(e)
{	
	$.ajax({
		url:"guildPerksResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#guildAchievementsResources-form").submit(function(e)
{	
	$.ajax({
		url:"guildAchievementsResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#itemClassesResources-form").submit(function(e)
{	
	$.ajax({
		url:"itemClassesResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#talentsResources-form").submit(function(e)
{	
	$.ajax({
		url:"talentsResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#petTypesResources-form").submit(function(e)
{	
	$.ajax({
		url:"petTypesResources_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

// Sc2 Data
$("#sc2Profiles-form").submit(function(e)
{	
	$.ajax({
		url:"sc2Profiles_ctrl",
		method: "post",
		data: {id: $("#id").val(), name: $("#name").val(), region: $("#region").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#sc2Ladders-form").submit(function(e)
{	
	$.ajax({
		url:"sc2Ladders_ctrl",
		method: "post",
		data: {id: $("#id").val(), name: $("#name").val(), region: $("#region").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#sc2MatchHistory-form").submit(function(e)
{	
	$.ajax({
		url:"sc2MatchHistory_ctrl",
		method: "post",
		data: {id: $("#id").val(), name: $("#name").val(), region: $("#region").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#sc2Ladder-form").submit(function(e)
{	
	$.ajax({
		url:"sc2Ladder_ctrl",
		method: "post",
		data: {id: $("#id").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#sc2AchievementsData-form").submit(function(e)
{	
	$.ajax({
		url:"sc2AchievementsData_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#sc2RewardsData-form").submit(function(e)
{	
	$.ajax({
		url:"sc2RewardsData_ctrl",
		method: "post",
		data: {locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

// Diablo3 Data
$("#diablo3CareerProfiles-form").submit(function(e)
{	
	$.ajax({
		url:"diablo3CareerProfiles_ctrl",
		method: "post",
		data: {battleTag: $("#battleTag").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#diablo3HeroProfiles-form").submit(function(e)
{	
	$.ajax({
		url:"diablo3HeroProfiles_ctrl",
		method: "post",
		data: {battleTag: $("#battleTag").val(), heroId: $("#heroId").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#diablo3Items-form").submit(function(e)
{	
	$.ajax({
		url:"diablo3Items_ctrl",
		method: "post",
		data: {itemData: $("#itemData").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			console.log(msg);
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#diablo3Follower-form").submit(function(e)
{	
	$.ajax({
		url:"diablo3Follower_ctrl",
		method: "post",
		data: {followerData: $("#followerData").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});

$("#diablo3Artisan-form").submit(function(e)
{	
	$.ajax({
		url:"diablo3Artisan_ctrl",
		method: "post",
		data: {artisanData: $("#artisanData").val(), locale: $("#locale").val(), jsonp: $("#jsonp").val(), _token: $("#token").val()},
	}).done(function(msg) 
	{
		if($("#jsonp").val() === "")
		{
			jMsg = JSON.parse(msg);
			var jsonString = JSON.stringify(jMsg, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jsonString+"</code></pre>";
		}
		else if($("#jsonp").val().length !== "")
		{	
			jPad_Begin = msg.substring(0, msg.indexOf("(") + 1);
			jPad_End = msg.substring(msg.lastIndexOf(")"), msg.length);
			jMsg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")"));
			jMsg_conv = JSON.parse(jMsg);
			var jsonString = JSON.stringify(jMsg_conv, null, '\t');
			document.getElementById("results").innerHTML = "<pre><code>"+jPad_Begin+jsonString+jPad_End+"</code></pre>";
		}
		else
		{
			document.getElementById("results").innerHTML = "<pre><code>"+msg+"</code></pre>";
		}
	});
	return false;
});