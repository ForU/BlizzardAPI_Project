<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">
        <title>Laravel</title>

		<!-- Bootstrap core CSS -->
		<link href="../../../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="../../../css/output.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
		<!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="../../../css/bootstrap/js/bootstrap.min.js"></script>
		<!-- Piwik -->
		<script type="text/javascript">
		  var _paq = _paq || [];
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
			var u="//tracklode.com/";
			_paq.push(['setTrackerUrl', u+'piwik.php']);
			_paq.push(['setSiteId', 1]);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		  })();
		</script>
		<noscript><p><img src="//tracklode.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
		<!-- End Piwik Code -->
		
		<nav class="navbar navbar-inverse navbar-fixed-top header">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">Project name</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
			  <ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="#about">About</a></li>
				<li><a href="#contact">Contact</a></li>
				<li><a href="/apiList_view">API List</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Character Profile<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="/characterProfile_view">Character Profile</a></li>
							<li><a href="/characterAchievements_view">Character Achievements</a></li>
							<li><a href="/characterAppearance_view">Character Achievements</a></li>
							<li><a href="/characterFeed_view">Character Feeds</a></li>
							<li><a href="/characterGuild_view">Character Guild</a></li>
							<li><a href="/characterHunterPets_view">Character Hunter Pets</a></li>
							<li><a href="/characterItems_view">Character Items</a></li>
							<li><a href="/characterMounts_view">Character Mounts</a></li>
							<li><a href="/characterPets_view">Character Pets</a></li>
							<li><a href="/characterPetSlots_view">Character Pet Slots</a></li>
							<li><a href="/characterProfessions_view">Character Professions</a></li>
							<li><a href="/characterProgression_view">Character Progression</a></li>
							<li><a href="/characterPVP_view">Character PVP</a></li>
							<li><a href="/characterQuests_view">Character Quests</a></li>
							<li><a href="/characterReputation_view">Character Reputation</a></li>
							<li><a href="/characterStatistics_view">Character Statistics</a></li>
							<li><a href="/characterStats_view">Character Stats</a></li>
							<li><a href="/characterTalents_view">Character Talents</a></li>
							<li><a href="/characterTitles_view">Character Titles</a></li>
							<li><a href="/characterAudit_view">Character Audit</a></li>
						</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Guild Profile<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="/guildProfile_view">Guild Profile</a></li>
						</ul>
				</li>
			  </ul>
			</div>
		  </div>
		</nav>
		
		<div class="container">
			<div class="content">
				{{-- dd($content) --}}
				@section('content')
					
				@show
			</div>
		</div>
		
		<!-- <footer class="footer">
			<div class="container">
				<p class="text-muted">Place sticky footer content here.</p>
				<p> Follow Us </p>
			</div>
		</footer> -->
		
		<footer class="footer">
			<div class="container">
			<table class="table footerSection1">
				<tbody>
					<tr>
						<td>
							<img class="footerlogo"></img>
						</td>
						<td class="labels">
							About Us
						</td>
						<td class="labels">
							Contact Us
						</td>
						<td class="labels">
							Site Map
						</td>
						<td class="labels">
							Terms of Use
						</td>
						<td class="labels">
							Follow Us
							<table class="socialIcons">
								<tbody>
									<tr>
										<td>
											<i class="fa fa-twitter fa-lg"></i>
										</td>
										<td>
											<i class="fa fa-facebook fa-lg"></i>
										</td>
										<td>
											<i class="fa fa-google-plus fa-lg"></i>
										</td>
										<td>
											<i class="fa fa-youtube fa-lg"></i>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table footerSection2">
				<tbody>
				<tr>
					<td>
						Conditions of Use
					</td>
					<td>
						Privacy Notice
					</td>
					<td>
						© 2016, Hearthlode.com
					</td>
				</tr>
				</tbody>
			</table>
			</div>
		</footer>
	<script type="text/javascript" src="../../../js/ajax_submission.js"></script>
    </body>
</html>
