@extends('layouts.test')

@section('content')
	@parent
	<form id="apiList-form" class="form-area" ng-submit="submit()">
		<label class="labels">API Category: </label> <select id="apiCategory" class="form-control"></select>
		<!-- <label class="labels">WoW API List: </label> <select id="wowApi" class="form-control"></select> -->
		<span id="gameApi"></span>
		<input class="btn btn-lg btn-primary btn-block submit-view" type="submit" id="submit" value="Submit" />
	</form>
	<span id="results"></span>

@endsection