@extends('layouts.test')

@section('content')
	@parent
	<form id="petAbility-form" class="form-area" ng-submit="submit()">
		<label class="labels"> Ability Id: </label><input id="abilityId" class="form-control" ></input><br>
		<label class="labels"> Locale: </label>
		<!-- <select id="locale" class="form-control" >
			<option value="en_US">en_US</option>
			<option value="pt_BR">pt_BR</option>
			<option value="es_MX">es_MX</option>
		</select> -->
		<select id="locale" class="form-control"></select>
		<br>
		<label class="labels"> JsonP: </label><input id="jsonp" class="form-control" ></input>
		<!-- <select id="jsonp" class="form-control"></select> -->
		<input id="token" class="form-control" type="hidden" name="_token" value="{{{csrf_token()}}}" />
		<input class="btn btn-lg btn-primary btn-block submit-view" type="submit" id="submit" value="Submit" />
	</form>
	<span id="results"></span>
	<table>
		<tbody class="db_table">
			
		</tbody>
	</table>
	
	<!-- An element a visitor can click if they <3 comments! -->
	<span class="show-comments btn btn-lg btn-primary btn-block ">Load comments</span>

	<!-- The empty element required for Disqus to loads comments into -->  
	<div id="disqus_thread"></div>
@endsection